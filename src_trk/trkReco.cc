#include "trkReco.h"
#include "ProcessPhiBin.h"
#include "trkConstants.h"

#ifndef __SYNTHESIS__
#include <iostream>
#endif

//--- Reconstruct tracks
void trkReco(ap_uint<NBITS_HIT> hits, ap_uint<1> last_hit, 
             ap_uint<1>& done_hits_read, ap_uint<1>& done_seed_reco, ap_uint<1>& done_trk_reco, 
             ap_uint<NBITS_DEPTH>& nTracks, 
             ap_uint<NBITS_TRK> tracks[ramDepth_]) {
#pragma HLS INTERFACE ap_none port=done_hits_read
#pragma HLS INTERFACE ap_none port=done_seed_reco
#pragma HLS INTERFACE ap_none port=done_trk_reco
#pragma HLS INTERFACE ap_none port=nTracks
  // Code below is only capable of accepting a new hit every 2 clock cycles.
#pragma HLS PIPELINE II=2

  //-------------------------------------------------------------------
  // THEORY
  // Eqn. of track circle: phi = phi0 - 0.5*r/R,
  // where track radius of curvature R = alpha*q*Pt 
  // where  alpha = B*c/1e9, r in metres, Pt in GeV & q=+-1.
  //
  // Look for tracks with Hough Transform counting stubs in 2D array
  // of (1/R, phi0)
  //-------------------------------------------------------------------

  static ProcessPhiBin processPhiBin[numPhiBinsL0];
#pragma HLS ARRAY_PARTITION variable=processPhiBin complete dim=1
  for (unsigned int i = 0; i < numPhiBinsL0; i++) processPhiBin[i].setBin(i);
  
  static ap_uint<NBITS_DEPTH+1> index = 0;
  static ap_uint<1> done_hits_read_tmp = false;
  static ap_uint<1> done_seed_reco_tmp = false;
  static ap_uint<1> done_trk_reco_tmp = false;

  if (not done_hits_read_tmp) {

    //-- Process additional hits

    // Unpack next hit.
    ap_uint<NBITS_PHI> phi = hit.range(MSB_PHI , LSB_PHI);
    ap_uint<16> phiBin = phi >> nPhiDiv;
#ifndef __SYNTHESIS__
    assert(phiBin < numPhiBinsL0); // If fails, increase nPhiDiv
#endif
    done_hits_read_tmp = processPhiBin[phiBin].storeHit(hit, last_hit);

  } else if (not done_seed_reco_tmp) {

    for (ap_uint<8> i = 0; i < numPhiBinsL0; i++) {
# pragma HLS unroll
      done_seed_reco_tmp = processPhiBin[i].findSeeds();
    }

  } else if (not done_trk_reco_tmp) {

    nTracks = 0;
    for (ap_uint<8> i = 0; i < numPhiBinsL0; i++) {
      processPhiBin[i].findTracks(tracks, nTracks); 
    }
   
    done_trk_reco_tmp = true;
  }
   
  done_hits_read = done_hits_read_tmp; // Finished reading hits
  done_seed_reco = done_seed_reco_tmp; // Finished reading hits
  done_trk_reco  = done_trk_reco_tmp; // Finished reconstructing tracks
}
  
