#ifndef __trkReco__
#define __trkReco__

#include "ap_int.h"

// Number of bits in hit word
#include "../src_common/hitFormat.h"
// Parameters defining size of BRAM memory for storing tracks
#include "../src_common/trkMemorySpec.h"

// Reconstruct tracks
//
// Input: hit (injected each clock cycle) & boolean indicating if no more
//        hits available.
// Output: doneHitsRead=1 when finished reading all hits;
//         done_trk_reco=1 when finished reconstructing tracks;
//         nTracks = number of tracks reconstructed;
//         tracks[] = reconstructed tracks.

void trkReco(ap_uint<NBITS_HIT> hit, ap_uint<1> last_hit, 
             ap_uint<1>& done_hits_read, ap_uint<1>& done_trk_reco, 
             ap_uint<NBITS_DEPTH>& nTracks, 
             ap_uint<NBITS_TRK> tracks[ramDepth_]);

#endif
