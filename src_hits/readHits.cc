#include "readHits.h"

void readHits(ap_uint<NBITS_HIT>& nextHit, ap_uint<1>& last) {

#pragma HLS INTERFACE ap_none port=last
#pragma HLS INTERFACE ap_none port=nextHit
// Determines how many clock cycles (function calls) the output data will 
// be held for before updating with a new value.
// Set this equal to the pipeline interval (II) achieved by src_trk/ .
#pragma HLS PIPELINE II=2
 
  static ap_uint<16> idx = 0;

  if (idx < numHits) {
    nextHit = hits[idx];
    last = (idx == numHits - 1) ? 1 : 0;
    idx++;

  } else {
    nextHit = nullHit;
    last = 1;
  }
}
