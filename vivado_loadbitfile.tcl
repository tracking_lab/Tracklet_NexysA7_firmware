###################################################
# Load .bit file compiled by vivado.tcl into FPGA #
###################################################

# Name of bit file
set file top.bit
# Name of device
set dev  xc7a100t_0

open_hw_manager
connect_hw_server
open_hw_target
current_hw_device [get_hw_devices xc7a100t_0]
set_property PROGRAM.FILE {WorkVHDL/WorkVHDL.runs/impl_1/top.bit} [get_hw_devices xc7a100t_0]
program_hw_devices [get_hw_devices xc7a100t_0]
refresh_hw_device [lindex [get_hw_devices xc7a100t_0] 0]
