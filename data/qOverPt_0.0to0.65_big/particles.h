const unsigned int nParticles=300;
float true_phi0   [nParticles] = {
    0.021,    0.035,    0.059,    0.066,    0.082,
    0.114,    0.126,    0.146,    0.166,    0.199,
    0.215,    0.232,    0.257,    0.269,    0.272,
    0.280,    0.281,    0.291,    0.322,    0.347,
    0.373,    0.380,    0.387,    0.418,    0.420,
    0.467,    0.476,    0.519,    0.535,    0.545,
    0.561,    0.584,    0.587,    0.598,    0.610,
    0.634,    0.648,    0.667,    0.719,    0.728,
    0.734,    0.768,    0.802,    0.837,    0.857,
    0.872,    0.884,    0.890,    0.904,    0.938,
    0.940,    0.974,    0.996,    1.014,    1.051,
    1.052,    1.060,    1.065,    1.085,    1.108,
    1.116,    1.116,    1.117,    1.129,    1.203,
    1.215,    1.229,    1.231,    1.267,    1.273,
    1.306,    1.307,    1.325,    1.347,    1.373,
    1.394,    1.438,    1.454,    1.468,    1.483,
    1.492,    1.520,    1.523,    1.542,    1.562,
    1.565,    1.598,    1.686,    1.736,    1.738,
    1.762,    1.816,    1.824,    1.844,    1.886,
    1.889,    1.899,    1.917,    1.920,    1.947,
    1.955,    1.959,    1.968,    2.015,    2.053,
    2.077,    2.080,    2.082,    2.115,    2.127,
    2.154,    2.202,    2.236,    2.242,    2.276,
    2.278,    2.279,    2.337,    2.395,    2.398,
    2.489,    2.523,    2.529,    2.532,    2.572,
    2.579,    2.580,    2.594,    2.601,    2.647,
    2.654,    2.664,    2.708,    2.727,    2.778,
    2.791,    2.884,    2.934,    2.961,    2.963,
    2.976,    3.002,    3.043,    3.057,    3.058,
    3.071,    3.083,    3.105,    3.106,    3.112,
    3.133,    3.179,    3.194,    3.220,    3.286,
    3.308,    3.318,    3.325,    3.359,    3.372,
    3.405,    3.414,    3.424,    3.441,    3.460,
    3.509,    3.552,    3.576,    3.596,    3.606,
    3.619,    3.670,    3.735,    3.763,    3.807,
    3.821,    3.830,    3.852,    3.867,    3.899,
    3.901,    3.902,    3.935,    3.938,    3.939,
    3.975,    3.991,    4.037,    4.094,    4.109,
    4.111,    4.114,    4.127,    4.160,    4.192,
    4.199,    4.219,    4.220,    4.252,    4.253,
    4.310,    4.381,    4.464,    4.496,    4.599,
    4.617,    4.679,    4.690,    4.693,    4.777,
    4.808,    4.808,    4.817,    4.823,    4.845,
    4.861,    4.862,    4.871,    4.879,    4.920,
    4.924,    4.944,    4.947,    4.969,    5.016,
    5.023,    5.029,    5.029,    5.033,    5.068,
    5.072,    5.083,    5.089,    5.106,    5.120,
    5.139,    5.200,    5.208,    5.214,    5.222,
    5.256,    5.263,    5.325,    5.328,    5.370,
    5.390,    5.433,    5.438,    5.451,    5.465,
    5.490,    5.493,    5.507,    5.516,    5.521,
    5.530,    5.538,    5.544,    5.563,    5.588,
    5.603,    5.629,    5.648,    5.653,    5.661,
    5.667,    5.680,    5.682,    5.711,    5.727,
    5.735,    5.768,    5.795,    5.798,    5.809,
    5.828,    5.828,    5.831,    5.872,    5.882,
    5.893,    5.927,    5.965,    5.990,    5.999,
    6.008,    6.031,    6.043,    6.043,    6.070,
    6.071,    6.085,    6.085,    6.102,    6.169,
    6.194,    6.206,    6.224,    6.266,    6.268,};
float true_tanL   [nParticles] = {
    0.351,    0.706,    0.794,   -0.265,    0.798,
    0.939,   -0.177,    0.795,   -0.192,   -0.688,
    0.208,    0.511,    0.785,    0.848,    0.896,
   -0.963,   -0.589,   -0.756,    0.219,    0.463,
    0.757,    0.388,    0.005,   -0.032,   -0.673,
    0.075,   -0.343,    0.961,   -0.392,   -0.687,
   -0.536,    0.822,    0.993,   -0.328,   -0.786,
   -0.390,    0.200,   -0.489,    0.569,    0.615,
    0.786,    0.209,    0.857,   -0.004,   -0.922,
    0.563,   -0.766,   -0.857,    0.245,   -0.979,
   -0.280,    0.632,    0.264,   -0.175,    0.120,
    0.409,   -0.571,    0.864,    0.900,   -0.358,
   -0.441,   -0.473,    0.226,    0.240,    0.338,
    0.529,   -0.089,   -0.813,   -0.772,    0.738,
    0.814,    0.798,    0.353,    0.263,    0.973,
   -0.901,    0.368,    0.346,   -0.792,   -0.215,
    0.081,    0.677,   -0.638,   -0.523,    0.878,
   -0.273,   -0.619,   -0.958,    0.651,    0.882,
   -0.673,   -0.299,   -0.105,   -0.577,    0.789,
   -0.638,    0.198,   -0.234,    0.243,   -0.533,
   -0.660,   -0.484,    0.088,   -0.011,    0.766,
   -0.713,    0.077,    0.418,    0.088,   -0.410,
   -0.582,    0.059,    0.776,   -0.910,   -0.856,
    0.602,   -0.433,    0.637,    0.997,    0.912,
   -0.870,    0.335,    0.380,    0.928,    0.027,
    0.174,    0.204,    0.535,    0.549,    0.049,
    0.971,    0.997,    0.161,   -0.477,    0.887,
   -0.367,    0.412,   -0.462,   -0.370,    0.275,
   -0.808,    0.042,    0.752,   -0.554,   -0.227,
   -0.419,   -0.808,   -0.215,    0.115,    0.017,
   -0.999,    0.146,    0.106,   -0.827,    0.502,
   -0.748,    0.516,   -0.197,    0.794,    0.762,
    0.422,    0.757,    0.849,    0.069,    0.228,
    0.754,   -0.121,   -0.011,    0.527,   -0.606,
   -0.470,    0.546,    0.858,    0.244,    0.485,
    0.184,    0.692,   -0.735,    0.543,    0.677,
    0.588,   -0.118,   -0.550,    0.089,   -0.989,
   -0.560,   -0.346,    0.155,    0.320,   -0.472,
    0.802,   -0.862,   -0.021,    0.883,    0.691,
    0.408,    0.836,   -0.931,    0.710,   -0.506,
    0.142,    0.762,   -0.685,    0.384,    0.302,
    0.125,    0.623,    0.349,   -0.443,   -0.767,
   -0.460,   -0.796,    0.472,    0.902,    0.928,
   -0.332,   -0.622,   -0.744,    0.156,   -0.271,
    0.611,   -0.693,    0.497,    0.394,   -0.250,
    0.222,    0.352,   -0.360,    0.065,   -0.397,
   -0.586,    0.557,    0.062,   -0.747,   -0.162,
   -0.124,   -0.224,   -0.186,    0.437,    0.059,
    0.590,    0.247,   -0.874,    0.809,    0.957,
   -0.460,   -0.782,    0.111,    0.269,   -0.658,
   -0.494,   -0.881,    0.713,    0.148,   -0.182,
   -0.125,    0.153,    0.029,    0.750,   -0.118,
   -0.409,   -0.478,   -0.877,   -0.253,   -0.717,
    0.197,    0.700,    0.009,   -0.556,    0.581,
   -0.120,   -0.798,   -0.875,   -0.703,    0.498,
   -0.064,    0.495,    0.113,    0.001,   -0.749,
    0.734,   -0.072,    0.863,   -0.620,   -0.381,
    0.199,    0.800,   -0.920,   -0.660,    0.462,
    0.219,   -0.816,    0.233,    0.152,   -0.904,
    0.283,    0.483,    0.964,   -0.885,    0.332,};
float true_qOverPt[nParticles] = {
    0.487,   -0.450,    0.460,    0.487,    0.266,
    0.210,   -0.018,    0.303,   -0.259,    0.053,
   -0.210,    0.319,   -0.508,   -0.054,    0.115,
   -0.167,   -0.618,    0.049,   -0.414,   -0.429,
    0.470,   -0.473,    0.109,   -0.053,    0.579,
    0.204,    0.258,   -0.188,   -0.515,   -0.553,
    0.364,    0.351,    0.462,   -0.120,    0.406,
    0.424,   -0.034,    0.559,    0.316,   -0.063,
   -0.575,    0.347,   -0.123,   -0.359,    0.443,
    0.591,   -0.636,    0.155,   -0.022,   -0.457,
    0.580,    0.201,   -0.268,    0.634,   -0.191,
   -0.290,    0.316,    0.187,    0.604,    0.407,
    0.065,   -0.194,   -0.210,    0.459,    0.580,
   -0.423,   -0.644,   -0.342,    0.632,   -0.382,
   -0.058,   -0.488,    0.178,    0.481,   -0.114,
   -0.044,   -0.636,    0.178,   -0.138,    0.395,
    0.242,   -0.495,    0.239,   -0.377,    0.444,
   -0.463,   -0.233,    0.085,    0.478,    0.618,
    0.647,   -0.014,    0.486,   -0.222,   -0.318,
    0.404,   -0.474,   -0.158,    0.096,    0.484,
    0.231,   -0.224,   -0.244,   -0.540,   -0.391,
   -0.465,   -0.232,   -0.040,   -0.178,    0.575,
    0.312,   -0.492,    0.194,    0.232,   -0.287,
   -0.515,   -0.551,   -0.139,   -0.017,    0.343,
    0.345,   -0.413,    0.051,    0.581,   -0.436,
   -0.086,   -0.013,    0.359,   -0.389,   -0.353,
    0.300,   -0.381,   -0.375,    0.386,   -0.114,
    0.202,    0.351,   -0.561,    0.563,    0.134,
   -0.647,   -0.040,   -0.448,    0.167,    0.415,
   -0.067,   -0.045,   -0.115,    0.634,   -0.572,
    0.243,    0.324,   -0.068,   -0.258,   -0.069,
    0.302,    0.575,    0.208,    0.476,   -0.637,
    0.288,   -0.379,    0.192,   -0.343,   -0.506,
    0.237,   -0.350,    0.613,    0.311,   -0.223,
    0.469,    0.517,    0.485,   -0.534,    0.438,
   -0.454,    0.304,   -0.011,    0.109,    0.378,
   -0.438,   -0.472,   -0.521,   -0.114,   -0.134,
    0.019,    0.180,    0.463,    0.458,    0.047,
    0.046,   -0.466,   -0.258,   -0.495,    0.474,
    0.284,   -0.079,    0.414,    0.245,    0.420,
    0.181,   -0.193,   -0.508,   -0.260,   -0.402,
   -0.056,    0.030,   -0.424,   -0.539,    0.174,
   -0.593,   -0.242,    0.395,    0.489,   -0.116,
   -0.030,    0.355,    0.120,    0.285,   -0.643,
    0.320,    0.464,    0.587,    0.303,    0.359,
   -0.111,   -0.318,    0.411,    0.132,    0.184,
   -0.424,   -0.363,   -0.224,    0.043,    0.249,
   -0.265,    0.092,   -0.171,    0.604,   -0.245,
    0.126,   -0.339,   -0.440,   -0.466,    0.012,
   -0.117,    0.600,   -0.600,    0.176,    0.024,
    0.177,    0.401,   -0.371,    0.074,    0.476,
    0.309,    0.612,    0.193,   -0.444,   -0.342,
    0.436,    0.310,    0.512,    0.552,    0.159,
    0.644,    0.195,    0.386,    0.260,    0.489,
    0.147,   -0.327,   -0.626,    0.265,   -0.100,
    0.114,   -0.065,    0.308,   -0.454,    0.430,
    0.516,   -0.520,    0.155,   -0.271,   -0.601,
   -0.225,   -0.453,    0.337,    0.072,   -0.141,
    0.350,   -0.097,   -0.558,   -0.571,    0.568,
   -0.310,    0.081,    0.391,    0.013,    0.517,};
float true_invR   [nParticles] = {
    0.584,   -0.540,    0.552,    0.585,    0.319,
    0.252,   -0.022,    0.364,   -0.310,    0.064,
   -0.252,    0.383,   -0.609,   -0.065,    0.137,
   -0.200,   -0.741,    0.059,   -0.497,   -0.515,
    0.565,   -0.568,    0.130,   -0.064,    0.695,
    0.245,    0.309,   -0.226,   -0.618,   -0.664,
    0.437,    0.421,    0.555,   -0.145,    0.488,
    0.509,   -0.041,    0.671,    0.379,   -0.076,
   -0.690,    0.417,   -0.148,   -0.431,    0.531,
    0.710,   -0.764,    0.186,   -0.026,   -0.548,
    0.696,    0.241,   -0.321,    0.761,   -0.229,
   -0.348,    0.380,    0.224,    0.724,    0.488,
    0.079,   -0.233,   -0.252,    0.550,    0.696,
   -0.508,   -0.773,   -0.411,    0.758,   -0.459,
   -0.070,   -0.586,    0.214,    0.577,   -0.137,
   -0.053,   -0.764,    0.213,   -0.165,    0.474,
    0.290,   -0.595,    0.287,   -0.453,    0.532,
   -0.556,   -0.279,    0.102,    0.574,    0.742,
    0.776,   -0.016,    0.583,   -0.266,   -0.382,
    0.484,   -0.569,   -0.190,    0.115,    0.580,
    0.277,   -0.269,   -0.293,   -0.648,   -0.469,
   -0.558,   -0.278,   -0.048,   -0.214,    0.690,
    0.375,   -0.591,    0.233,    0.278,   -0.345,
   -0.618,   -0.662,   -0.167,   -0.020,    0.411,
    0.414,   -0.496,    0.062,    0.697,   -0.523,
   -0.103,   -0.016,    0.431,   -0.467,   -0.424,
    0.359,   -0.458,   -0.450,    0.463,   -0.136,
    0.243,    0.422,   -0.673,    0.675,    0.161,
   -0.776,   -0.048,   -0.537,    0.201,    0.498,
   -0.080,   -0.055,   -0.138,    0.761,   -0.687,
    0.291,    0.388,   -0.082,   -0.310,   -0.083,
    0.363,    0.691,    0.249,    0.571,   -0.765,
    0.346,   -0.454,    0.231,   -0.411,   -0.607,
    0.284,   -0.420,    0.736,    0.373,   -0.268,
    0.563,    0.620,    0.582,   -0.641,    0.526,
   -0.545,    0.365,   -0.014,    0.131,    0.453,
   -0.525,   -0.566,   -0.625,   -0.137,   -0.161,
    0.023,    0.216,    0.556,    0.549,    0.056,
    0.055,   -0.559,   -0.309,   -0.594,    0.569,
    0.341,   -0.095,    0.496,    0.293,    0.504,
    0.217,   -0.232,   -0.610,   -0.312,   -0.483,
   -0.067,    0.036,   -0.509,   -0.647,    0.208,
   -0.712,   -0.290,    0.474,    0.587,   -0.139,
   -0.036,    0.426,    0.144,    0.342,   -0.772,
    0.384,    0.557,    0.705,    0.363,    0.431,
   -0.134,   -0.382,    0.493,    0.159,    0.221,
   -0.509,   -0.436,   -0.268,    0.052,    0.298,
   -0.318,    0.111,   -0.205,    0.724,   -0.294,
    0.151,   -0.407,   -0.528,   -0.559,    0.014,
   -0.140,    0.720,   -0.720,    0.211,    0.029,
    0.212,    0.481,   -0.445,    0.088,    0.572,
    0.371,    0.734,    0.231,   -0.533,   -0.410,
    0.523,    0.372,    0.614,    0.663,    0.191,
    0.773,    0.234,    0.463,    0.312,    0.587,
    0.177,   -0.392,   -0.751,    0.317,   -0.119,
    0.136,   -0.078,    0.370,   -0.545,    0.516,
    0.619,   -0.624,    0.186,   -0.325,   -0.721,
   -0.270,   -0.544,    0.404,    0.087,   -0.169,
    0.420,   -0.117,   -0.669,   -0.685,    0.682,
   -0.372,    0.097,    0.469,    0.016,    0.620,};
