const unsigned int nParticles=30;
float true_phi0   [nParticles] = {
    0.418,    0.545,    0.648,    0.667,    0.974,
    1.085,    1.117,    1.215,    1.347,    1.394,
    1.483,    1.738,    1.947,    2.489,    2.523,
    3.179,    3.372,    3.807,    3.939,    4.220,
    4.599,    4.808,    5.029,    5.029,    5.068,
    5.208,    5.370,    5.530,    5.768,    5.828,};
float true_tanL   [nParticles] = {
   -0.053,   -0.986,    0.696,   -0.153,   -0.677,
   -0.838,    0.038,   -0.710,   -0.174,   -0.447,
    0.269,   -0.798,   -0.445,   -0.756,   -0.082,
    0.792,   -0.964,    0.550,   -0.413,   -0.416,
   -0.337,   -0.090,    0.994,    0.963,   -0.773,
    0.191,    0.092,    0.995,    0.548,   -0.044,};
float true_qOverPt[nParticles] = {
    0.067,    0.197,    0.335,    0.194,   -0.205,
    0.306,    0.425,   -0.441,    0.140,    0.238,
    0.137,   -0.049,   -0.383,    0.608,   -0.037,
    0.621,   -0.195,   -0.273,    0.133,   -0.170,
   -0.016,    0.244,    0.376,   -0.281,   -0.050,
   -0.631,    0.383,    0.088,   -0.272,    0.165,};
float true_invR   [nParticles] = {
    0.081,    0.236,    0.402,    0.233,   -0.246,
    0.367,    0.510,   -0.529,    0.168,    0.285,
    0.165,   -0.059,   -0.459,    0.730,   -0.045,
    0.745,   -0.234,   -0.327,    0.159,   -0.204,
   -0.019,    0.292,    0.451,   -0.338,   -0.060,
   -0.757,    0.459,    0.106,   -0.326,    0.198,};
