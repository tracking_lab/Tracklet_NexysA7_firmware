const unsigned int nParticles=3;
float true_phi0   [nParticles] = {
    0.648,    1.483,    2.489,};
float true_tanL   [nParticles] = {
   -0.690,    0.099,   -0.556,};
float true_qOverPt[nParticles] = {
    0.601,   -0.603,    0.605,};
float true_invR   [nParticles] = {
    0.721,   -0.723,    0.726,};
