
#include "ap_int.h"


#define INPUT_CANT_BITS 27

typedef ap_uint<4> bcd_t;
typedef ap_uint<INPUT_CANT_BITS> in_data_t;

void bin2bcd(in_data_t, bcd_t*, int);
