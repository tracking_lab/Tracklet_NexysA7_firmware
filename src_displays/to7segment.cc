#include <stdint.h>
#include "bin2bcd.h"
#include "to7segment.h"


                                         // [MSB: dot, g, f, e, d, c, b, LSB:a]
static const segment_7_t segment_7[10] = {0b00111111,
                                          0b00000110,
                                          0b01011011,
                                          0b01001111,
                                          0b01100110,
                                          0b01101101,
                                          0b01111101,
                                          0b00000111,
                                          0b01111111,
                                          0b01100111};

segment_7_t to7segment(bcd_t in_data)
{
	return segment_7[in_data];
}
