#include "ap_int.h"
#include "bin2bcd.h"
#include "to7segment.h"

#define BITS_DISPLAY_COUNTER 3
#define NUM_OF_DISPLAYS (1 << BITS_DISPLAY_COUNTER)
#define BITS_CLOCK_COUNTER 10

typedef ap_uint<BITS_DISPLAY_COUNTER> display_counter_t;
typedef ap_uint<BITS_CLOCK_COUNTER> clock_counter_t;
typedef ap_uint<NUM_OF_DISPLAYS> display_sel_t;

void disp_nexys_a7(in_data_t value,display_sel_t* psel, segment_7_t* pdisplay);
