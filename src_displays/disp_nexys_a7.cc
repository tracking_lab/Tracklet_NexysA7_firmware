#include "bin2bcd.h"
#include "to7segment.h"
#include "disp_nexys_a7.h"

void disp_nexys_a7(in_data_t value,display_sel_t* psel, segment_7_t* pdisplay)
{
#pragma HLS INTERFACE ap_none port=psel
#pragma HLS INTERFACE ap_none port=pdisplay
#pragma HLS PIPELINE II=1

  static clock_counter_t    clock_counter   = 0;
  static display_counter_t  display_counter = 0;
  static bcd_t bcd_values[NUM_OF_DISPLAYS];
  
  display_sel_t sel;
  segment_7_t display;
    
  if( clock_counter == 0 )
  {
    if (display_counter == 0)
    {
      bin2bcd(value,bcd_values,NUM_OF_DISPLAYS);
    }
    display_counter++;
  }
  clock_counter++;

  display = to7segment(bcd_values[display_counter]);
  sel = 1 << display_counter;

  *psel = sel;
  *pdisplay = display;
}
